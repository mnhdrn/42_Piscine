/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/09 13:21:29 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/11 00:26:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_iterative_power(int nb, int power)
{
	int		i;
	int		temp;

	temp = nb;
	i = 0;
	if (power == 0)
		return (1);
	if (power > 0)
	{
		while (i < power - 1)
		{
			nb *= temp;
			i++;
		}
		return (nb);
	}
	else
		return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 00:48:00 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/11 11:19:00 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_prime(int nb)
{
	int		i;

	i = 2;
	while ((i * i) <= nb && i < 100)
	{
		if (nb % i == 0)
			return (1);
		i++;
	}
	return (nb);
}

int			ft_find_next_prime(int nb)
{
	int		tmp;

	tmp = 0;
	if (nb <= 2)
		return (2);
	else
	{
		while (ft_is_prime(nb) == 1)
		{
			nb += 1;
		}
		return (nb);
	}
	return (nb);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nxt_one.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 13:54:46 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/24 13:54:48 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tail.h"

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

void			ft_putstr(char *str)
{
	int			i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

char			*ft_strcpy(char *dest, char *src)
{
	int			i;

	i = -1;
	while (dest[++i])
		dest[i] = src[i];
	dest[i] = '\0';
	return (dest);
}

char			*ft_strcat(char *dest, char *src)
{
	int			i;
	int			size;

	i = 0;
	size = 0;
	while (dest[size])
		size++;
	while (src[i])
	{
		dest[size + i] = src[i];
		i++;
	}
	dest[size + i] = '\0';
	return (dest);
}

int				ft_strlen(char *str)
{
	int			i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

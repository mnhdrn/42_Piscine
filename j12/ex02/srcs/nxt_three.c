/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nxt_three.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 16:09:17 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/25 15:16:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tail.h"

void				ft_csize(char *str, int n)
{
	int				i;
	int				k;
	int				len;

	len = ft_strlen(str);
	i = len;
	k = 0;
	while (k < n && i != 0)
	{
		k++;
		i--;
	}
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void				ft_cend(char *str)
{
	int				i;
	int				k;
	int				len;

	len = ft_strlen(str);
	i = len;
	k = 0;
	while (k < 11 && i != 0)
	{
		if (str[i] == '\n')
			k++;
		i--;
	}
	i += 2;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void				ft_cfront(char *str, int n)
{
	int				i;
	int				len;
	char			*final;

	i = 0;
	n = n - 1;
	len = ft_strlen(str);
	final = (char *)malloc(sizeof(char) * (len + 1));
	if (len > n)
	{
		while (str[i])
		{
			final[i] = str[n + i];
			i++;
		}
		final[i] = '\0';
	}
	ft_putstr(final);
}

void				ft_caller_one(int ac, char **av)
{
	if (ac == 2)
	{
		ft_putstr(ft_strdup("tail: option requires an argument -- c\n"));
		ft_putstr(ft_strdup("usage: tail [-c] [file ...]\n"));
	}
	else
	{
		if (ft_is_alpha(av[2]) == 1)
		{
			ft_putstr(ft_strdup("tail: illegal offset -- \0"));
			ft_putstr(av[2]);
			ft_putchar('\n');
		}
		else if (ft_is_alpha(av[2]) == 0)
		{
			ft_caller_two(ac, av);
		}
	}
}

void				ft_caller_two(int ac, char **av)
{
	if (ac == 4)
	{
		if (av[2][0] == '+')
			ft_reader(av[3], 1, ft_atoi(av[2]), 0);
		else
			ft_reader(av[3], 2, ft_atoi(av[2]), 0);
	}
	else if (ac > 4)
	{
		if (av[2][0] == '+')
			ft_multiplay_two(ac, av, 1);
		else
			ft_multiplay_two(ac, av, 2);
	}
}

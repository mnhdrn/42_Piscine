/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 15:31:45 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/23 10:15:56 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display.h"

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

void			ft_putstr(char *str)
{
	int			i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int				main(int ac, char **av)
{
	int			fd;
	int			size;
	char		buffer[128 + 1];

	if (ac < 2)
		write(2, "File name missing.\n", 19);
	else if (ac > 2)
		write(2, "Too many arguments.\n ", 20);
	else
	{
		fd = open(av[1], O_RDONLY);
		while ((size = read(fd, buffer, 128)) != 0)
		{
			buffer[size] = '\0';
			ft_putstr(buffer);
		}
		close(fd);
	}
	return (0);
}

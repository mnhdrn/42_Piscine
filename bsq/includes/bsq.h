/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 16:21:25 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/26 13:54:55 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# define SIZE 524288

typedef struct	s_type
{
	char		full;
	char		empty;
	char		ob;
	size_t		size;
}				t_type;

typedef struct	s_square
{
	size_t		x;
	size_t		y;
	int			size;
	char		*str;
}				t_square;

typedef struct	s_map
{
	size_t		l_size;
	size_t		ligne;
	size_t		nb_ligne;
}				t_map;

bool			algo(char *str, int *tab, t_type *chara, t_square *square);
int				ft_strlen(char *str);
void			ft_putstr(char *str);
char			*ft_realloc(char *str, int size);
void			ft_strcat(char *dest, char *src);
int				ft_strcmp(char *s1, char *s2);
int				ft_atoi(char *str);
void			ft_memset(void *data, char value, size_t size);
char			*opti_read(int fd, int *s_total, char *path);

#endif

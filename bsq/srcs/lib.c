/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 02:56:25 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/24 17:58:59 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int					ft_strlen(char *str)
{
	size_t			i;

	i = 0;
	if (str == NULL)
		return (0);
	while (str[i])
		i++;
	return (i);
}

void				ft_putstr(char *str)
{
	size_t			i;

	i = 0;
	while (str[i])
		i++;
	write(1, str, i);
}

char				*ft_realloc(char *str, int size_total)
{
	char			*new;
	size_t			i;

	i = 0;
	if ((new = malloc(sizeof(char) * (size_total + 1))) == NULL)
		return (NULL);
	while (str && str[i])
	{
		new[i] = str[i];
		i++;
	}
	new[i] = 0;
	free(str);
	return (new);
}

void				ft_strcat(char *dest, char *src)
{
	int				i;
	int				j;

	j = 0;
	i = 0;
	while (dest[i])
		i++;
	while (src[j])
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = 0;
}

int					ft_atoi(char *str)
{
	unsigned int	nb;
	int				signe;
	int				i;

	signe = 1;
	i = 0;
	nb = 0;
	while (str[i] == '\r' || str[i] == '\v' || str[i] == '\f' ||
			str[i] == '\n' || str[i] == '\t' || str[i] == ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			signe = -1;
		i++;
	}
	while (str[i] && str[i] >= '0' && str[i] <= '9')
	{
		nb = nb * 10 + str[i] - '0';
		i++;
	}
	return ((int)nb * signe);
}

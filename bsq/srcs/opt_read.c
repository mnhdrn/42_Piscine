/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opt_read.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/26 13:33:07 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/26 13:55:27 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include "bsq.h"

char			*opti_read(int fd, int *s_total, char *path)
{
	char		buff[SIZE];
	int			size_total;
	int			size;
	char		*all_read;

	size_total = 0;
	while ((size = read(fd, buff, SIZE)) > 0)
		size_total += size;
	close(fd);
	if ((fd = open(path, O_RDONLY)) < 0)
		return (NULL);
	all_read = malloc(sizeof(char) * (size_total + 1));
	size = read(fd, all_read, size_total);
	all_read[size] = 0;
	close(fd);
	*s_total = size_total;
	return (all_read);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval_expr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 11:46:33 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/22 16:41:58 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "eval.h"

int					ft_atoi(char *str)
{
	unsigned int	nb;
	int				signe;
	int				i;

	signe = 1;
	i = 0;
	nb = 0;
	while (str[i] == '\r' || str[i] == '\v' || str[i] == '\f' ||
			str[i] == '\n' || str[i] == '\t' || str[i] == ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			signe = -1;
		i++;
	}
	while (str[i] && str[i] >= '0' && str[i] <= '9')
	{
		nb = nb * 10 + str[i] - '0';
		i++;
	}
	return ((int)nb * signe);
}

int					ulti_getnbr(char **str)
{
	int				nbr;
	int				i;
	char			*tmp;

	i = 0;
	tmp = *str;
	while (tmp[i] == '\n' || tmp[i] == '\t' || tmp[i] == ' ')
		i++;
	if (tmp[i] == '(')
	{
		nbr = eval_expr(&tmp[1]);
		while (tmp[i] != ')')
			i++;
		i++;
	}
	else
	{
		nbr = ft_atoi(tmp);
		while (tmp[i] >= '0' && tmp[i] <= '9')
			i++;
	}
	while (tmp[i] == '\n' || tmp[i] == '\t' || tmp[i] == ' ')
		i++;
	*str = &tmp[i];
	return (nbr);
}

int					calc_add(int nbr, char signe, char *str)
{
	int				b;

	b = ulti_getnbr(&str);
	if (*str == '*' || *str == '/' || *str == '%')
		b = calc_mul(b, *str, &str[1]);
	if (signe == '+')
		b = nbr + b;
	else if (signe == '-')
		b = nbr - b;
	if (*str == '-' || *str == '+')
		b = calc_add(b, *str, &str[1]);
	return (b);
}

int					calc_mul(int nbr, char signe, char *str)
{
	int				b;

	b = ulti_getnbr(&str);
	if (signe == '/')
		b = nbr / b;
	else if (signe == '%')
		b = nbr % b;
	else if (signe == '*')
		b = nbr * b;
	if (*str == '*' || *str == '/' || *str == '%')
		b = calc_mul(b, *str, &str[1]);
	else if (*str == '-' || *str == '+')
		b = calc_add(b, *str, &str[1]);
	return (b);
}

int					eval_expr(char *str)
{
	int				a;
	int				i;

	i = 0;
	while (str[i] == '\r' || str[i] == '\v' || str[i] == '\f' ||
			str[i] == '\n' || str[i] == '\t' || str[i] == ' ')
		i++;
	str = &str[i];
	a = ulti_getnbr(&str);
	if (*str && (*str == '/' || *str == '*' || *str == '%'))
		a = calc_mul(a, *str, &str[1]);
	else if (*str)
		a = calc_add(a, *str, &str[1]);
	return (a);
}

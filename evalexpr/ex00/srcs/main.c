/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 11:44:38 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/22 12:05:17 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "eval.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	rec_put(unsigned int nbr)
{
	if (nbr >= 10)
		rec_put(nbr / 10);
	ft_putchar(nbr % 10 + '0');
}

void	ft_putnbr(int nbr)
{
	if (nbr < 0)
	{
		ft_putchar('-');
		rec_put(nbr * -1);
	}
	else
		rec_put(nbr);
}

int		main(int ac, char **av)
{
	if (ac > 1)
	{
		ft_putnbr(eval_expr(av[1]));
		ft_putchar('\n');
	}
	return (0);
}

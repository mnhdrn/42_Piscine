/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_any.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 09:33:46 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/23 09:06:05 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_any(char **tab, int (*f)(char *))
{
	int			i;

	i = 0;
	while (tab[i] != 0)
	{
		if (f(tab[i]) == 1)
			return (1);
		i++;
	}
	return (0);
}

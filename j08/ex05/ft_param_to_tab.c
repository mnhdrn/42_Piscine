/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 00:26:02 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/20 15:19:48 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_par.h"

int						ft_strlen(char *str)
{
	int					i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char					*ft_strdup(char *src)
{
	int					i;
	int					len;
	char				*dest;

	len = ft_strlen(src);
	if (!(dest = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

struct s_stock_par		ft_stock_new(char *src)
{
	t_stock_par			tab;

	tab.size_param = ft_strlen(src);
	tab.str = src;
	tab.copy = ft_strdup(src);
	tab.tab = ft_split_whitespaces(src);
	return (tab);
}

struct s_stock_par		*ft_param_to_tab(int ac, char **av)
{
	t_stock_par			*tab;
	int					i;

	if (!(tab = (t_stock_par *)malloc(sizeof(t_stock_par) * (ac + 1))))
		return (NULL);
	i = 0;
	while (i < ac)
	{
		tab[i] = ft_stock_new(av[i]);
		i++;
	}
	tab[i].str = 0;
	return (&tab[0]);
}

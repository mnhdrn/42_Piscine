/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 12:04:15 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/12 18:20:44 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int						ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int		i;
	int					result;

	i = 0;
	result = 0;
	while (i < n)
	{
		if (s1[i] == s2[i])
		{
			i++;
		}
		else
			return (s1[i] - s2[i]);
	}
	return (0);
}

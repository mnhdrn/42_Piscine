/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 11:29:54 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/11 12:12:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_strcmp(char *s1, char *s2)
{
	int		i;
	int		result;

	i = 0;
	result = 0;
	while (s1[i] == s2[i] && s1[i])
		i++;
	result = s1[i] - s2[i];
	return (result);
}

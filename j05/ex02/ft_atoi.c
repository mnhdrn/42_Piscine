/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/10 14:48:37 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/15 22:31:50 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int					ft_atoi(char *str)
{
	unsigned int	nbr;
	int				i;
	int				negative;

	i = 0;
	negative = 0;
	nbr = 0;
	while (str[i] == '\t' || str[i] == '\v' || str[i] == '\f' ||
			str[i] == '\r' || str[i] == '\n' || str[i] == ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			negative = 1;
		i++;
	}
	while (str[i] && (str[i] >= '0' && str[i] <= '9'))
	{
		nbr = (nbr * 10) + (str[i] - '0');
		i++;
	}
	if (negative == 1)
		return ((int)nbr * -1);
	else
		return ((int)nbr);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/07 13:53:37 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/10 11:13:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_calcul(int i, int goal, char *str)
{
	if (str[i] == ' ' || str[i] == '\t' || str[i] == '\v' ||
		str[i] == '\f' || str[i] == '\r' || str[i] == '\n')
		i++;
	if (str[i] >= '0' && str[i] <= '9')
	{
		while (str[i])
		{
			if (str[i] >= '0' && str[i] <= '9')
			{
				goal *= 10;
				goal += (int)str[i] - 48;
			}
			else
				return (0);
			i++;
		}
	}
	return (goal);
}

int		ft_atoi(char *str)
{
	int		i;
	int		negative;
	int		atoi;

	if (str[0] == '-')
		i = 1;
	else
		i = 0;
	negative = i;
	atoi = ft_calcul(i, 0, str);
	if (negative == 1)
		atoi = atoi * -1;
	return (atoi);
}

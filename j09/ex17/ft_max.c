/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 11:12:17 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/14 11:13:14 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_max(int *tab, int length)
{
	int	i;
	int	m;
	int	temp;

	m = 0;
	i = m + 1;
	while (m < length - 1)
	{
		while (i < length)
		{
			if (tab[i] < tab[min])
			{
				temp = tab[i];
				tab[i] = tab[m];
				tab[m] = temp;
			}
			i++;
		}
		m++;
		i = m + 1;
	}
	i = 0;
	while (i < length - 1)
		i++;
	return (tab[i]);
}

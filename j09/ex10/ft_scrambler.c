/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_scrambler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 11:03:13 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/14 11:03:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_scrambler(int ***a, int *b, int *******c, int ****d)
{
	int tampa;
	int tampb;
	int tampc;
	int tampd;

	tampa = ***a;
	tampb = *b;
	tampc = *******c;
	tampd = ****d;
	*******c = tampa;
	****d = tampc;
	*b = tampd;
	***a = tampb;
}

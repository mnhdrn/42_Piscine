/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjoncture.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 10:58:28 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/14 10:59:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int		ft_collatz_conjecture(unsigned int base)
{
	if (base == 1)
		return (0);
	if ((base % 2) == 0)
		base = base / 2;
	else
		base = (base * 3) + 1;
	return (ft_collatz_conjecture(base) + 1);
}

int					main(void)
{
	unsigned int	x;
	int				i;

	x = 127;
	i = ft_collatz_conjecture(x);
	return (i);
}

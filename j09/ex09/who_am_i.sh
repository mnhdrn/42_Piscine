#!/bin/bash

vaip=$(ifconfig | grep inet);

if [[ -z $vaip ]]
then
    echo -e "Je suis perdu!"
else
    echo -e $vaip | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b"
fi

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_active_bits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 11:07:05 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/14 11:36:34 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int		ft_active_bits(int value)
{
	unsigned int count;

	count = 0;
	while (value)
	{
		count = count + (value & 1);
		value >>= 1;
	}
	return (count);
}

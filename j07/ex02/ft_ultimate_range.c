/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/15 13:40:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/19 18:27:49 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			*ft_range(int min, int max)
{
	int		*tab;
	int		i;

	if (min >= max)
		return (NULL);
	if (!(tab = (int *)malloc(sizeof(int) * (max - min))))
		return (0);
	i = 0;
	while (min < max)
	{
		tab[i] = min;
		min++;
		i++;
	}
	return (tab);
}

int			ft_ultimate_range(int **range, int min, int max)
{
	int		*tab;

	min = (min < 0 && max < 0) ? (min * -1) : min;
	max = (max < 0 && min < 0) ? (max * -1) : max;
	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	tab = ft_range(min, max);
	*range = tab;
	return (max - min);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:44:56 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/08 14:53:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_display(int x, char c1, char c2, char c3)
{
	int			i;

	ft_putchar(c1);
	i = 0;
	while (i < (x - 2))
	{
		if (x <= 2)
			ft_putchar(c1);
		else
			ft_putchar(c2);
		i++;
	}
	if (x >= 2)
		ft_putchar(c3);
	ft_putchar('\n');
}

void		rush(int x, int y)
{
	int		i;

	if (y > 0 && x > 0)
	{
		y = y - 2;
		ft_display(x, 'o', '-', 'o');
		i = 0;
		while (i < y)
		{
			ft_display(x, '|', ' ', '|');
			i++;
		}
		if (y >= 1)
			ft_display(x, 'o', '-', 'o');
	}
}
